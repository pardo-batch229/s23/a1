let trainer = {
    name: "Ash Ketchum",
    age: 10,
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends:{
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    talk: function(){
        console.log("Pikachu! I choose you!")
    }
}
// Ouput of trainer Object
console.log(trainer);
// Result of using dot.notation
console.log("Result of dot notation:")
console.log(trainer.name);
// Result of using bracket[] notation
console.log("Result of bracket notation:")
console.log(trainer["pokemon"]);
// Result of talk method
console.log("Result of talk method")
trainer.talk();

function Pokemon(name,level){
    this.name = name;
    this.level = level;
    health = level * 3;
    attack = level * 1.5;

    this.tackle = function(targetPokemon){
        console.log(`${this.name} tackled ${targetPokemon.name}` )
    }
    this.fainted = function(){
        console.log(`${this.name} has fainted`);
    }
}

let pokemon1 = new Pokemon("Pikachu",10);
console.log(pokemon1);

let pokemon2 = new Pokemon("Geodude", 8);
console.log(pokemon2);

let pokemon3 = new Pokemon("Mew", 100);
console.log(pokemon3);


pokemon1.tackle(pokemon2);
pokemon2.tackle(pokemon3);
pokemon3.fainted();

